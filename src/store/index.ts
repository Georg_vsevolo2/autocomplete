import Vue from 'vue';
import Vuex from 'vuex';
import api from '../api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoading: false,
    users: [],
    photoArray: [],
    photoSet: new Map,
  },

  getters: {
    getPhotoById: state => (id: any) => state['photoSet'].get(id),
  },

  actions: {
    async getPhotos({commit, state}, params = {}) {
      await api.getUsersData('photos')
        .then((photos) => {
          const MapData = new Map();
          photos.data.map((photo: { id: any; }) => MapData.set(photo.id, photo))
          return MapData;
        })
        .then(data => commit('setPhotos', data))
    },
    async getUsers({commit, state}, params = {}) {
      try {
        commit('setState', true)
        const urlParam = '?name_like=' + params.text;
        await api.getUsersData('users' + urlParam)
          .then((users: any) => {
            commit('setUsers', users);
          })
      } catch (e) {
        alert(e)
      } finally {
        commit('setState', false)
      }
    }
  },
  mutations: {
    setPhotos (state, photos) {
      state.photoSet = photos;
    },
    setUsers (state, users) {
      state.users = users.data
    },
    setState (state, value) {
      state['isLoading'] = value
    }
  },
  strict: true
});
