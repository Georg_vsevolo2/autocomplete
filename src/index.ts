import './style.scss'
import Vue from 'vue'
import App from './components/App.vue'
import store from './store'
import { router } from './router'

const vm = new Vue({
  render: h => h(App),
  router,
  store
})

vm.$mount('#app')
