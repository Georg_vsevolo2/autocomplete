import axios from 'axios'

/**
 * @var {Axios}
 */
const instance = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/'
})

/**
 * Load data.
 *
 * @url {String} url
 * @params {Object} params
 * @returns {Promise}
 */
function getUsersData(url = '', params = {}) {
  return instance.request({
    method: 'get',
    url: url,
    params
  })
}

export default {
  instance,
  getUsersData
}
