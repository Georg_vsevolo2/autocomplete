import {Component, Vue} from 'vue-property-decorator'

import store from "../store"

@Component<HomeComponent>({
  beforeRouteEnter(_f, _t, next) {
    store.dispatch('getPhotos').then(() => next());
  },
})

export class HomeComponent extends Vue {
  title = ''
  body = ''
}
